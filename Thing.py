class Thing:
    def __init__(self,volume,name = None):
        self._volume = volume
        self._name = name

    def get_volume(self):
        return self._volume

    def get_name(self):
        return self._name

    def has_name(self,truc):
        return truc == self.get_name()

    def set_name(self,truc):
        self._name = truc

    def __repr__(self):
        return "Thing(" + str(self.get_volume()) + "," + self.get_name() + ")"
